import * as sdk from 'cyborg/sdk'

const onServerStarted = async (bp: typeof sdk) => {}
const onServerReady = async (bp: typeof sdk) => {}

const entryPoint: sdk.ModuleEntryPoint = {
  onServerStarted,
  onServerReady,
  definition: {
    name: 'basic-module',
    menuIcon: 'none',
    menuText: 'BasicExample',
    noInterface: false,
    fullName: 'BasicExample',
    homepage: 'https://cyborg.com'
  }
}

export default entryPoint
