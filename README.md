# [Cyborg](https://cyborg.com/?utm_source=github&utm_medium=organic&utm_campaign=cyborg_repo&utm_term=readme) — The building blocks for building chatbots

## What is Cyborg ?
Cyborg is the standard developer stack to build, run and improve Conversational-AI applications. Powered by natural language understanding, a messaging API and a fully featured studio, Cyborg allows developers around the globe to build remarkable chatbots without compromise.
a
<a href='https://cyborg.com/?utm_source=github&utm_medium=organic&utm_campaign=cyborg_repo&utm_term=readme'><img src='.github/assets/studio.png'></a>

**Out of the box, Cyborg includes:**

- Administration panel to orchestrate and monitor your chatbots
- Conversation Studio to design a conversation, manage content, code custom integration
- Easy integration with messaging channels (Messenger, WhatsApp, Slack, Teams, Webchat, Telegram, SMS & more)
- Natural Language Understanding

## Getting Started

There are a few ways to get started with cyborg :

- Download the latest binary for your OS [here](https://cyborg.com/download?utm_source=github&utm_medium=organic&utm_campaign=cyborg_repo&utm_term=readme) and follow the [installation docs](https://cyborg.com/docs/installation).
- Use the official [Docker image](https://hub.docker.com/r/cyborg/server) and follow the [hosting docs](https://cyborg.com/docs/infrastructure/Docker)
- Deploy it in the cloud using these shortlinks:
  [![DigitalOcean](.github/do_button.svg)](https://marketplace.digitalocean.com/apps/cyborg) [![Deploy](https://www.herokucdn.com/deploy/button.svg)](https://heroku.com/deploy)
- Run from sources, follow [build docs](https://cyborg.com/docs/infrastructure/deploying)

## Documentation

- [Main Documentation](https://cyborg.com/docs/introduction)
- [SDK Reference](https://cyborg.com/reference/)
- [Code Examples](https://github.com/cyborg/cyborg/tree/master/examples)
- [Video Tutorials](https://www.youtube.com/c/cyborg)

## Community

- [Community Forum](https://forum.cyborg.com/) - Get community support and discuss your challenges
- [Issues](https://github.com/cyborg/cyborg/issues) - Report bug and file feature requests
- [Blog](https://cyborg.com/blog) - How to's, Case studies & Announcements
- [Newsletter](https://cyborg.activehosted.com/f/16) - Stay informed, subscribe to our news letter
- [Contributring](/.github/CONTRIBUTING.md) - Start contributing to Cyborg
- [Partners](/.github/PARTNERS.md) - List of agencies who can help you with Cyborg

## License

Cyborg is dual-licensed under [AGPLv3](/licenses/LICENSE_AGPL3) and the [Cyborg Proprietary License](/licenses/LICENSE_cyborg).

By default, any bot created with Cyborg is licensed under AGPLv3, but you may change to the Cyborg License from within your bot's web interface in a few clicks.

For more information about how the dual-license works and why it works that way, please see the <a href="https://cyborg.com/faq">FAQS</a>.
