import { BotConfig } from 'cyborg/sdk'
import { cyborgConfig } from 'core/config'
import _ from 'lodash'
import moment from 'moment'
import ms from 'ms'

export interface DialogExpiry {
  context: Date
  session: Date
}

/**
 * Create expiry dates for dialog session and dialog context based on the bot configuration.
 * If no configuration is found for the bot, it will fallback to cyborg config.
 *
 * @param botConfig The bot configuration file i.e. bot.config.json
 * @param cyborgConfig Cyborg configuration file i.e. cyborg.config.json
 */
export function createExpiry(botConfig: BotConfig, cyborgConfig: cyborgConfig): DialogExpiry {
  const contextTimeout = ms(_.get(botConfig, 'dialog.timeoutInterval', cyborgConfig.dialog.timeoutInterval))
  const sessionTimeout = ms(
    _.get(botConfig, 'dialog.sessionTimeoutInterval', cyborgConfig.dialog.sessionTimeoutInterval)
  )

  return {
    context: moment()
      .add(contextTimeout, 'ms')
      .toDate(),
    session: moment()
      .add(sessionTimeout, 'ms')
      .toDate()
  }
}
