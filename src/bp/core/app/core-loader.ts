import 'bluebird-global'
import LicensingService from 'common/licensing-service'
import { GhostService } from 'core/bpfs'
import { ConfigProvider } from 'core/config'
import Database from 'core/database'
import { LoggerProvider } from 'core/logger'
import { LocalActionServer as LocalActionServerImpl } from 'core/user-code'
import { FatalError } from 'errors'
import 'reflect-metadata'

import { Cyborg as Core } from './cyborg'
import { container } from './inversify/app.inversify'
import { TYPES } from './types'

export interface cyborgApp {
  cyborg: Core
  logger: LoggerProvider
  config: ConfigProvider
  ghost: GhostService
  database: Database
  localActionServer: LocalActionServerImpl
}

export function createApp(): cyborgApp {
  try {
    const app = {
      cyborg: container.get<Core>(TYPES.Cyborg),
      logger: container.get<LoggerProvider>(TYPES.LoggerProvider),
      config: container.get<ConfigProvider>(TYPES.ConfigProvider),
      ghost: container.get<GhostService>(TYPES.GhostService),
      database: container.get<Database>(TYPES.Database),
      localActionServer: container.get<LocalActionServerImpl>(TYPES.LocalActionServer),
      licensing: container.get<LicensingService>(TYPES.LicensingService)
    }

    app.licensing.installProtection()

    return app
  } catch (err) {
    throw new FatalError(err, 'Error during initialization')
  }
}

export function createLoggerProvider(): LoggerProvider {
  return container.get<LoggerProvider>(TYPES.LoggerProvider)
}
