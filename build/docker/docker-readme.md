![cyborg](https://cyborg.com/blog/content/images/2017/06/xnobg_primary_black.png.pagespeed.ic.siY4jfFl48.png)

### Installation

Create persistent storage to keep cyborg data:

```bash
docker volume create cyborg_data
```

---

Start the container with minimal necessary options:

```bash
docker run --detach \
           --name=cyborg \
           --publish 3000:3000 \
           --volume cyborg_data:/cyborg/data \
           cyborg/server:latest
```

---

There are some predefined defaults. However, you can specify ones via environmental variables:

```bash
docker run --detach \
           --name=cyborg \
           --publish 3000:8080 \
           --volume cyborg_data:/cyborg/data \
           --env  PORT=8080 \ # Don't forget to adjust "--publish" then
           --env  BP_HOST=0.0.0.0 \ # all zeroes means listen to all interfaces
           --env  NODE_ENV=production \
           --env  PG_HOST=192.168.0.11 \
           --env  PG_PORT=5432 \
           --env  PG_USER=bp_user \
           --env  PG_PASSWORD=<********> \
           --env  PG_SSL=false \
           cyborg/server:latest
```

---

Now you can track the logs:

```bash
docker logs --follow cyborg
```

---

If you wish to connect to the running container:

```bash
docker exec --interactive --tty cyborg bash
```

---

Full documentation resource is available on the [official website](https://cyborg.com/docs/).
[Changelog resides here](https://github.com/cyborg/cyborg/blob/master/CHANGELOG.md).
