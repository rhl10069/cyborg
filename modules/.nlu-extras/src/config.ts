import { DialogflowConfig } from './backend/typings'

export interface Config {
  /**
   * Specify the primary NLU engine that will be used by Cyborg.
   *
   * NOTE: Cyborg NLU always run and can't be disabled. If another NLU is specified as primary,
   * it will run after Cyborg and will overwrite the prediction results.
   * @default 'cyborg-nlu'
   */
  primary: 'cyborg-nlu' | 'dialogflow-nlu'

  /**
   * Configuration of the Dialogflow NLU engine
   * @default null
   */
  dialogflow?: DialogflowConfig
}
