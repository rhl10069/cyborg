# @cyborg/analytics

Analytics for Cyborg provides an interface to view graphs and data of your chatbot typical usage. By using this module, you can have a look to your total users, retention, daily active users, busy hours and a lot more...

## Usage

This module has some built-in analytics available from the box.

## License

cyborg-analytics is licensed under [AGPL-3.0](/LICENSE)
