module.exports = {
  copyFiles: [
    'src/cyborg.d.ts',
    'src/typings/node.d.txt',
    'src/typings/es6include.txt',
    'src/typings/bot.config.schema.json',
    'src/typings/cyborg.config.schema.json'
  ]
}

const fs = require('fs')
const path = require('path')

fs.mkdirSync('dist')
fs.copyFileSync(path.join(__dirname, '../../src/bp/sdk/cyborg.d.ts'), path.join(__dirname, 'dist/cyborg.d.js'))
