# cyborg-audience

Official Audience Viewer for [Cyborg](http://github.com/cyborg/cyborg).

This module has been build to facilitate the views of your users information.

### Community

There's a [Community Forum](https://forum.cyborg.com) where you are welcome to join us, ask any question and even help others.

### License

cyborg-audience is licensed under AGPL-3.0
