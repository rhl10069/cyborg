---
id: about-pro
title: About
---

## Additional Features

| Feature                    | Community | Enterprise |
| -------------------------- | --------- | ---------- |
| Cyborg NLU               | ✔️        | ✔️         |
| Visual Flow Editor         | ✔️        | ✔️         |
| Dialog Management          | ✔️        | ✔️         |
| Bot CMS                    | ✔️        | ✔️         |
| Unlimited Bots             | ✔️        | ✔️         |
| Unlimited Admins           |           | ✔️         |
| White-Label Chat Interface |           | ✔️         |
| Role-Based Access Control  |           | ✔️         |
| High-Availability          |           | ✔️         |
| Increased Performance      |           | ✔️         |
| Standard Support           |           | ✔️         |
| Multilanguage Bots         |           | ✔️         |

For more details, [contact sales team](https://cyborg.com/request-demo/).

## Activation

To enable Cyborg Enterprise features simply set `pro.enabled` to `true` in your `cyborg.config.json` file

## White Label

There is an [example on our GitHub repository](https://github.com/cyborg/cyborg/tree/master/examples/whitelabel) on how to customize Cyborg for white-labeling.
