---
id: installation
title: Installation
---

Getting started with Cyborg is easy. We build and distribute binaries of the latest stable version and nightly builds of the Github master branch.

## Download

The latest stable binaries are available for download [**here**](https://cyborg.com/download). Alternatively, you may find all the versions and nightly builds in our public [**S3 Bucket**](https://s3.amazonaws.com/cyborg-binaries/index.html).

## Installation

To install Cyborg, unzip the file you download somewhere on your computer. Make sure that your computer has at least:

- Memory (RAM): Recommended 4 GB or above.
- Hard Drive: Recommended 64 GB of free space or above.
- A 64 bits architecture
- The right to read/write to the Cyborg directory and subdirectories.

## Starting Cyborg

### Executable

To start Cyborg, all you have to do is double click on the `bp` file in the directory you extracted Cyborg.

Alternatively, you can also start it from the terminal using the command:

```bash
./bp
```
The first time you run Cyborg, the built-in modules take some time to install. Thereafter, subsequent runs will be much faster.

Once the modules are installed and loaded, you should see something similar to the screenshot below.

![First Run](assets/server-start.png)

### Commands

You may also start Cyborg Server using the Command Line Interface (CLI). To see all the commands available, run `./bp --help`.

![CLI Start](assets/cli-help.png)

## Learn More

Here is a video tutorial to help you set up Cyborg on your computer. You can slow it down a bit to follow along.

- [Setting up on Windows](https://youtu.be/xf246NQyMj4)
- [Setting up on Mac](https://youtu.be/SBv0QOXyHL4)
- [Setting up on Linux](https://youtu.be/89RJx0kQyKM)
