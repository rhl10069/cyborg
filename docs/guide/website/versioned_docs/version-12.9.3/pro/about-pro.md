---
id: version-12.9.3-about-pro
title: About
original_id: about-pro
---

## Additional Features

| Feature                    | Community | Enterprise |
| -------------------------- | --------- | --- |
| Cyborg NLU               | ✔️        | ✔️  |
| Visual Flow Editor         | ✔️        | ✔️  |
| Dialog Management          | ✔️        | ✔️  |
| Bot CMS                    | ✔️        | ✔️  |
| Unlimited Bots             | ✔️        | ✔️  |
| Unlimited Admins           |           | ✔️  |
| White-Label Chat Interface |           | ✔️  |
| Role-Based Access Control  |           | ✔️  |
| High-Availability          |           | ✔️  |
| Increased Performances     |           | ✔️  |
| Standard Support           |           | ✔️  |
| Multilanguage Bots         |           | ✔️  |

For more details, [contact sales team](https://cyborg.com/request-demo/).

## Activation

To enable Cyborg Enterprise features simply set `pro.enabled` to `true` in your `cyborg.config.json` file
