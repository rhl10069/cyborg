---
id: version-12.0.3-licensing
title: Licensing
original_id: licensing
---

## Obtaining a License

Please contact sales@cyborg.io to obtain a license.

## Activate your License

When starting Cyborg, use the `BP_LICENSE_KEY=<license_key>` environment variable.

**Binary**

```bash
BP_LICENSE_KEY=<license_key> ./bp
```

**Docker**

```bash
docker run -d \
--name cyborg \
-p 3000:3000 \
-v cyborg_data:/cyborg/data \
-e BP_LICENSE_KEY=<license_key>
cyborg/server:$TAG
```

## License Breach and Details

If you happen to breach your license (e.g you try to add more nodes than allowed on your license), your bots won't work anymore until you either update your license or get back to an unbreached license state.

If you don't remember the limits of your currently activated license, you can always go to the **Server Settings** page in your user top right menu. There you'll have all information on your current license.

![BP license keys active](assets/bp-server-settings.jpg)
