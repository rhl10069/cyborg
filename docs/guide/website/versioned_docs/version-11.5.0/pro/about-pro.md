---
id: version-11.5.0-about-pro
title: About
original_id: about-pro
---

## Features

[TODO] List pro features meanwhile see [list on pricing page](https://cyborg.com/pricing/)

## Activate pro

To enable Cyborg Pro features simply set `pro.enabled` to `true` in your cyborg.config.json file
