---
id: version-12.3.0-about-pro
title: About
original_id: about-pro
---

## Additional Features

| Feature                    | Community | Pro |
| -------------------------- | --------- | --- |
| Cyborg NLU               | ✔️        | ✔️  |
| Visual Flow Editor         | ✔️        | ✔️  |
| Dialog Management          | ✔️        | ✔️  |
| Bot CMS                    | ✔️        | ✔️  |
| Unlimited Bots             | ✔️        | ✔️  |
| Unlimited Admins           |           | ✔️  |
| White-Label Chat Interface |           | ✔️  |
| RBAC                       |           | ✔️  |
| High-Availability          |           | ✔️  |
| Increased Performances     |           | ✔️  |
| Standard Support           |           | ✔️  |
| Multilanguage Bots         |           | ✔️  |

For more details, visit our [pricing page](https://cyborg.com/pricing/).

## Activation

To enable Cyborg Pro features simply set `pro.enabled` to `true` in your cyborg.config.json file
