/**
 * Copyright (c) 2017-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

// See https://docusaurus.io/docs/site-config for all the possible
// site configuration options.

const siteConfig = {
  title: "| Developer's Guide",
  tagline: 'Guides and references for all you need to know about Cyborg',
  url: 'https://cyborg.com/docs',
  baseUrl: '/',
  repoUrl: 'https://github.com/cyborg/cyborg',
  projectName: 'cyborg-docs',
  organizationName: 'cyborg',

  algolia: {
    apiKey: '570227d66d130d069630e7226c740158',
    indexName: 'cyborg',
    algoliaOptions: {
      facetFilters: ['version:VERSION']
    }
  },

  docsSideNavCollapsible: true,
  headerLinks: [
    { doc: 'introduction', label: 'Docs' },
    { href: 'https://cyborg.com/reference/', label: 'SDK' },
    { href: 'https://forum.cyborg.com/', label: 'Community' },
    { href: 'https://github.com/cyborg/cyborg', label: 'Github' },
    { search: true }
  ],

  headerIcon: 'img/cyborg_icon.svg',
  footerIcon: 'img/cyborg.svg',
  favicon: 'img/favicon.png',

  colors: {
    primaryColor: '#165FFB',
    secondaryColor: '#cfcfcf'
  },

  copyright: `Copyright © ${new Date().getFullYear()} Cyborg, Inc.`,

  highlight: {
    theme: 'default'
  },

  // Add custom scripts here that would be placed in <script> tags.
  scripts: ['https://buttons.github.io/buttons.js', '/js/hotjar.js'],

  // On page navigation for the current documentation page.
  onPageNav: 'separate',
  // No .html extensions for paths.
  cleanUrl: true,

  // Open Graph and Twitter card images.
  ogImage: 'img/docusaurus.png',
  twitterImage: 'img/docusaurus.png',

  editUrl: 'https://github.com/cyborg/cyborg/edit/master/docs/guide/docs/',
  gaTrackingId: 'UA-90034220-1',
  gaGtag: true
}

module.exports = siteConfig
