/**
 * Copyright (c) 2017-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

const React = require('react')

class Footer extends React.Component {
  docUrl(doc, language) {
    const baseUrl = this.props.config.baseUrl
    return `${baseUrl}docs/${language ? `${language}/` : ''}${doc}`
  }

  pageUrl(doc, language) {
    const baseUrl = this.props.config.baseUrl
    return baseUrl + (language ? `${language}/` : '') + doc
  }

  render() {
    return (
      <footer className="nav-footer" id="footer">
        <section className="sitemap">
          <div>
            <h5>Docs</h5>
            <a href={this.docUrl('introduction')}>Getting Started</a>
            <a href="https://cyborg.com/reference/">API Reference</a>
          </div>
          <div>
            <h5>Community</h5>
            <a href="https://forum.cyborg.com/" target="_blank" rel="noreferrer noopener">
              forum.cyborg.com
            </a>
            <a href="https://stackoverflow.com/search?q=cyborg" target="_blank" rel="noreferrer noopener">
              Stack Overflow
            </a>
          </div>
          <div>
            <h5>More</h5>
            <a href="https://github.com/cyborg">GitHub</a>
            <a
              className="github-button"
              href={this.props.config.repoUrl}
              data-icon="octicon-star"
              data-count-href="/cyborg/cyborg/stargazers"
              data-show-count="true"
              data-count-aria-label="# stargazers on GitHub"
              aria-label="Star this project on GitHub"
            >
              Star
            </a>
            <a href="https://twitter.com/getcyborg">Twitter</a>
          </div>
        </section>

        <a href="https://cyborg.com/" target="_blank" rel="noreferrer noopener" className="fbOpenSource">
          <img src={`${this.props.config.baseUrl}img/cyborg.svg`} alt="Cyborg, Inc." width="170" height="45" />
        </a>
        <section className="copyright">{this.props.config.copyright}</section>
      </footer>
    )
  }
}

module.exports = Footer
